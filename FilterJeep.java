package filter;

public class FilterJeep implements Filter{

    @Override
    public List<Cars> filter(List<Cars> carsList) {
        List<Cars> jeepCars = new ArrayList<>();

        for(Cars cars : carsList) {
            if(cars.getType().equalsIgnorcase("Jeep")) {
                jeepCars.add(cars);
            }
        }
        return jeepCars;
    }
}
