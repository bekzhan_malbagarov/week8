package filter;

public class DoubleFilter implements Filter {

    private Filter firstFilter;
    private Filter secondFilter;

    public DoubleFilter(Filter firstFilter,Filter secondFilter) {
        this.firstFilter = firstFilter;
        this.secondFilter = secondFilter;
    }


    @Override
    public List<Cars> filter(List<Cars> carsList) {
        List<Cars> firstCarFilter = firstFilter.filter(carsList);
        return secondFilter.filter(firstCarFilter)
    }

}
