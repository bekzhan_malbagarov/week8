package filter;

public class MainFilter {
    public static void main(String[] args) {
        List<Cars>carsList = new ArrayList<>();

        carsList.add(new Cars("Toyota Camry","Sedan",2.0));
        carsList.add(new Cars("Toyota Highlander","Jeep",3.0));
        carsList.add(new Cars("Mersedes G-class","Jeep",5.5));
        carsList.add(new Cars("Audi A8","Sedan",3.5));
        carsList.add(new Cars("Reno Duster","Jee",2.5));



        Filter jeepCars = new FilterJeep();
        Filter sedanCars = new FilterSedan();
        Filter lowVolumeCars = new FilterVolumeLow();
        Filter highVolumeCars = new FilterVolumeHigh();


        Filter jeepLow = new DoubleFilter(jeepCars,lowVolumeCars);
        Filter jeepHigh = new DoubleFilter(jeepCars,highVolumeCars);
        Filter sedanLow = new DoubleFilter(sedanCars,lowVolumeCars);
        Filter sedanHigh = new DoubleFilter(sedanCars,highVolumeCars);


        System.out.println("Jeep cars: ");
        printCarsInfo(jeepCars.filter(carsList));

        System.out.println("Sedan cars: ");
        printCarsInfo(sedanCars.filter(carsList));

        System.out.println("Low volume cars: ");
        printCarsInfo(lowVolumeCars.filter(carsList));

        System.out.println("High Volume cars: ");
        printCarsInfo(highVolumeCars.filter(carsList));

    }
    public static void printCarsInfo(List<Cars> carsList) {
        for(Cars cars: carsList) {
            System.out.println("Car info: \nName:" + cars.getName() +",Type: "+ cars.getType() + ",Volume: "+ cars.getVolume()+ "\n");
        }
    }
}
