package filter;

public class FilterVolumeHigh Filter{

    @Override
    public List<Cars> filter(List<Cars> carsList) {
        List<Cars> highVolume = new ArrayList<>();

        for(Cars cars : carsList) {
            if(cars.getVolume()>3.0) {
                highVolume.add(cars);
            }
        }
        return highVolume;
    }
}
