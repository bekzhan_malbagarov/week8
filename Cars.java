package filter;

public class Cars {
    private String name;
    private String type;
    private double volume;

    public Cars(String name,String type,double volume){
        this.name = name;
        this.type = type;
        this.volume = volume;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public double getVolume() {
        return volume;
    }
}
