package builder;

public class HouseBuilder {
    private String adress;
    private int floornum;
    private String type;

    public HouseBuilder setAdress(String adress) {
        this.adress = adress;
        return this;
    }

    public HouseBuilder setFloornum(int floornum) {
        this.floornum = floornum;
        return  this;
    }

    public HouseBuilder setType(String type) {
        this.type = type;
        return this;
    }
}
