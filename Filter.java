package filter;

public interface Filter {
    public  List<Cars> filter(List<Cars> carsList);
}
