package filter;

public class FilterVolumeLow  Filter{

    @Override
    public List<Cars> filter(List<Cars> carsList) {
        List<Cars> lowVolume = new ArrayList<>();

        for(Cars cars : carsList) {
            if(cars.getVolume()<3.0) {
                lowVolume.add(cars);
            }
        }
        return lowVolume;
    }
}
