package builder;

public class House {
    private String address;
    private int floornum;
    private String type;

    public House(String address, int floornum, String type){
        super();
        this.address = address;
        this.floornum = floornum;
        this.type = type;
    }
}
